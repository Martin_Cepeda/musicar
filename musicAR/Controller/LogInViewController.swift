
import Foundation
import UIKit
import Lottie

class LogInViewController: UIViewController {
    
    @IBOutlet weak var LogInButton: UIButton!
    
    
    @IBOutlet var waveAnimate: AnimationView!
    
    
    var loginUrl: URL?
    var auth = SPTAuth.defaultInstance()!
    var didLoginSuccessfully: ((SPTSession, String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSpotify()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateAfterFirstLogin), name: NSNotification.Name(rawValue: "loginSuccessful"), object: nil)
        startAnimation()
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
       return .portrait
    }


    
    func startAnimation() {
        waveAnimate.animation = Animation.named("wave")
        waveAnimate.loopMode = .repeat(Float.infinity)
        waveAnimate.play()
    }
    
    @objc func updateAfterFirstLogin() {
        let userDefaults = UserDefaults.standard
        if let sessionObj: AnyObject = userDefaults.object(forKey: "SpotifySession") as AnyObject? {
            let sessionDataObj = sessionObj as! Data
            do {
                let firstTimeSession = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(sessionDataObj) as! SPTSession
                let session = firstTimeSession
                didLoginSuccessfully?(session, auth.clientID)
                dismiss(animated: true )
            }
            catch {
                print("error")
            }
            
            
        }
    }
    
    @IBAction func logInTapped(_ sender: Any) {
        
        if UIApplication.shared.openURL(loginUrl!) {
            if auth.canHandle(auth.redirectURL) {

            }
        }
    }
    
    func setupSpotify() {
        let redirectURL = "Music-AR://returnAfterLogin"
        let clientID = "f7d3897c6cb64b279c00294194900e79"
        auth.redirectURL = URL(string: redirectURL)
        auth.clientID = clientID
        auth.requestedScopes = [SPTAuthStreamingScope]
        loginUrl = auth.spotifyAppAuthenticationURL()
    }
    
    
    
    
}
