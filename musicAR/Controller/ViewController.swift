
import UIKit
import Photos
import AudioKit
import AudioKitUI
import SafariServices
import AVFoundation

import SceneKit
import ARKit
import SceneKitVideoRecorder
//import ReplayKit

class ViewController: UIViewController, ARSCNViewDelegate, UISearchBarDelegate, SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate {
    
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didChangeVolume volume: SPTVolume) {
        print(volume)
    }
    
    
    @IBOutlet var sceneView: ARSCNView!
    
    var session:SPTSession!
    var sptPlayer:SPTAudioStreamingController?
    
    var songs = [Song]()
       
    let tableView = UITableView()
    let tableContainer = UIView()
    let musicSearch:UISearchBar = UISearchBar()
    var clientID: String?
    var analyzer: AudioKitObejct!
    var shapes: Shape!
    
    var playbackView = PlaybackControlsView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AKSettings.audioInputEnabled = true
        analyzer = AudioKitObejct()
        AudioKit.output = analyzer.silence
        shapes = Shape(tracker_: analyzer.tracker)
        do {
            try AudioKit.start()
        } catch {
            AKLog("AudioKit did not start!")
        }
        Timer.scheduledTimer(timeInterval: 0.3,
                             target: self,
                             selector: #selector(ViewController.updateUI),
                             userInfo: nil,
                             repeats: true)
        //AR
        // Set the view's delegate
        sceneView.delegate = self
        
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        
        // Set the scene to the view
        sceneView.scene = scene
        setupLayout()
        setupTableView()
        addRecordButtonTarget()
        setupPlaybackView()
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
       return .portrait
    }
    
    @objc func updateUI() {
        if shapes.tracker.amplitude > 0.05 {
            shapes.hoverUpDown(collectionAR: shapes.sphereArray)
            shapes.makeLarge(collectionAR: shapes.sphereArray)
            shapes.rotateObject(collectionAR: shapes.sphereArray)
            shapes.rotateTorus(collectionAR: shapes.torusArray)
            shapes.changingAbstract(collectionAR: shapes.shadersArray)
        }
    }
    

        let torusButton:UIButton = UIButton()
        
        func setUpTorusButton() {
            torusButton.frame = CGRect(x: view.frame.maxX - view.frame.maxX * 0.1 , y: view.frame.minY + view.frame.maxY*0.15, width: 23, height: 23)
            torusButton.layer.cornerRadius = self.torusButton.frame.size.width/2.0
            torusButton.setImage(UIImage(named: "torus"), for: .normal)
            torusButton.layer.shadowOpacity = 0.2
            torusButton.layer.shadowRadius = 8.0
            torusButton.layer.shadowColor = UIColor.black.cgColor
            torusButton.layer.shadowOffset = CGSize(width: 0, height: 0)
            torusButton.tintColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
            torusButton.addTarget(self, action:#selector(torusShape), for: .touchDown)
        }
        
        let sphereButton:UIButton = UIButton()
        
        func setUpSphereButton() {
            sphereButton.frame = CGRect(x: view.frame.maxX - view.frame.maxX * 0.1 , y: view.frame.minY + view.frame.maxY*0.2, width: 23, height: 23)
            sphereButton.layer.cornerRadius = self.sphereButton.frame.size.width/2.0
            sphereButton.setImage(UIImage(named: "sphere"), for: .normal)
            sphereButton.layer.shadowOpacity = 0.2
            sphereButton.layer.shadowRadius = 8.0
            sphereButton.layer.shadowColor = UIColor.black.cgColor
            sphereButton.layer.shadowOffset = CGSize(width: 0, height: 0)
            sphereButton.addTarget(self, action:#selector(sphereShape), for: .touchDown)
        }
        
        let abstractButton:UIButton = UIButton()
        
        func setUpAbstractButton() {
            abstractButton.frame = CGRect(x: view.frame.maxX - view.frame.maxX * 0.1 , y: view.frame.minY + view.frame.maxY*0.25, width: 23, height: 23)
            abstractButton.layer.cornerRadius = self.sphereButton.frame.size.width/2.0
            abstractButton.setImage(UIImage(named: "abstract"), for: .normal)
            abstractButton.layer.shadowOpacity = 0.2
            abstractButton.layer.shadowRadius = 8.0
            abstractButton.layer.shadowColor = UIColor.black.cgColor
            abstractButton.layer.shadowOffset = CGSize(width: 0, height: 0)
            abstractButton.tintColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
            abstractButton.addTarget(self, action:#selector(abstractShape), for: .touchDown)
        }
        
        let clearButton: UIButton = UIButton()
        
        func setUpClear() {
            clearButton.frame = CGRect(x: view.frame.maxX - view.frame.maxX * 0.1 , y: view.frame.minY + view.frame.maxY*0.3, width: 23, height: 23)
            clearButton.layer.cornerRadius = self.sphereButton.frame.size.width/2.0
            clearButton.clipsToBounds = true
            clearButton.setImage(UIImage(named: "clear"), for: .normal)
            clearButton.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
            clearButton.addTarget(self, action:#selector(clearAll), for: .touchDown)
        }
        
        @objc func clearAll() {
            shapes.shadersArray.removeAll()
            shapes.torusArray.removeAll()
            shapes.sphereArray.removeAll()
            sceneView.scene.rootNode.enumerateChildNodes { (node, stop) in
            node.removeFromParentNode() }
        }
        
        
        @objc func torusShape() {
            shapeToMake = shapeChosen.torus
            isActive(shapeButton: torusButton)
            notActive(shapeButton: sphereButton)
            notActive(shapeButton: abstractButton)
        }
        
        @objc func sphereShape() {
            shapeToMake = shapeChosen.sphere
            isActive(shapeButton: sphereButton)
            notActive(shapeButton: torusButton)
            notActive(shapeButton: abstractButton)
        }
        
        @objc func abstractShape() {
            shapeToMake = shapeChosen.abstract
            isActive(shapeButton: abstractButton)
            notActive(shapeButton: sphereButton)
            notActive(shapeButton: torusButton)
        }
        
        func isActive(shapeButton: UIButton) {
            shapeButton.tintColor = UIColor(red:1.00, green:0.38, blue:0.27, alpha:1.0)
        }
        
        func notActive(shapeButton: UIButton) {
            shapeButton.tintColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
        }
    
    
    enum shapeChosen {
        case sphere
        case torus
        case abstract
    }
    
    var shapeToMake = shapeChosen.sphere

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        let result = sceneView.hitTest(touch.location(in: sceneView), types: [ARHitTestResult.ResultType.featurePoint])
        guard let hitResult = result.last else {return}
        let hitTransform = SCNMatrix4.init(hitResult.worldTransform)
        let hitVector = SCNVector3(hitTransform.m41, hitTransform.m42, hitTransform.m43)
        var currentNode = SCNNode()
        switch shapeToMake {
            case .sphere:
                currentNode = shapes.createSphere(position: hitVector)
            case .torus:
                currentNode = shapes.createTorus(position: hitVector)
            case .abstract:
                currentNode = shapes.createAbstract(position: hitVector)

        }
        sceneView.scene.rootNode.addChildNode(currentNode)
    }
    
    func setupPlaybackView() {
        view.insertSubview(playbackView, belowSubview: tableContainer)
        playbackView.horizontalSuperView()
        playbackView.bottomToSuperviewSafeArea()
        
        playbackView.playButton.addTarget(self, action: #selector(togglePlaying), for: .touchUpInside)
    }
    
    @objc func togglePlaying() {
        if playbackView.isPlaying {
            sptPlayer?.setIsPlaying(false, callback: nil)
            playbackView.changePlayButton(to: "play")
        } else {
            sptPlayer?.setIsPlaying(true, callback: nil)
            playbackView.changePlayButton(to: "pause")
        }
        playbackView.isPlaying = !playbackView.isPlaying
    }
    
    var didLogin = false
    
    override func viewDidAppear(_ animated: Bool) {
        if didLogin == false {
            let logInViewController = self.storyboard!.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
            logInViewController.didLoginSuccessfully = { [weak self] (session, clientId) in
                self?.didLogin = true
                self?.session = session
                self?.clientID = clientId
                self?.setupAfterLogin()
            }
            present(logInViewController, animated: true, completion: nil)
        }
    }
    
    func setupAfterLogin() {
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        } catch _ {
        }
        if sptPlayer == nil {
            sptPlayer = SPTAudioStreamingController.sharedInstance()
            sptPlayer!.playbackDelegate = self
            sptPlayer!.delegate = self
            if let clientId = clientID {
                try! sptPlayer!.start(withClientId: clientId)
            }
            sptPlayer!.login(withAccessToken: session.accessToken)
        }
    }
    
    func playSong(uri: String) {
        self.sptPlayer?.playSpotifyURI(uri, startingWith: 0, startingWithPosition: 8, callback: { (error) in
            if (error != nil) {
                print("playing!")
            }
        })
    }
    
    
    
    // MARK: - ReplayKit
    
    var recorder: SceneKitVideoRecorder?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if recorder == nil {
            var options = SceneKitVideoRecorder.Options.default

            let scale = UIScreen.main.nativeScale
            let sceneSize = sceneView.bounds.size
            options.videoSize = CGSize(width: sceneSize.width * scale, height: sceneSize.height * scale)
            recorder = try! SceneKitVideoRecorder(withARSCNView: sceneView, options: options)
        }
    }
    
    var isRecording = false
    
    @objc func recordScreen() {
        if !isRecording {
            isRecording = true
            changeRecordButton(to: "recording")
            self.recorder?.startWriting().onSuccess {
                print("Recording Started")
            }
        }
        else {
            isRecording = false
            changeRecordButton(to: "record")
            self.recorder?.finishWriting().onSuccess { [weak self] url in
                print("Recording Finished", url)
                self?.checkAuthorizationAndPresentActivityController(toShare: url, using: self!)
            }
        }
    }
    
    func addRecordButtonTarget() {
        playbackView.recordButton.addTarget(self, action: #selector(recordScreen), for: .touchUpInside)
    }
    
    private func checkAuthorizationAndPresentActivityController(toShare data: Any, using presenter: UIViewController) {

        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            let activityViewController = UIActivityViewController(activityItems: [data], applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.openInIBooks, UIActivity.ActivityType.print]
            presenter.present(activityViewController, animated: true, completion: nil)
        case .restricted, .denied:
            let libraryRestrictedAlert = UIAlertController(title: "Photos access denied",
                                                           message: "Please enable Photos access for this application in Settings > Privacy to allow saving screenshots.",
                                                           preferredStyle: UIAlertController.Style.alert)
            libraryRestrictedAlert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            presenter.present(libraryRestrictedAlert, animated: true, completion: nil)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (authorizationStatus) in
                if authorizationStatus == .authorized {
                    let activityViewController = UIActivityViewController(activityItems: [data], applicationActivities: nil)
                    activityViewController.excludedActivityTypes = [UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.openInIBooks, UIActivity.ActivityType.print]
                    DispatchQueue.main.async {
                        presenter.present(activityViewController, animated: true, completion: nil)
                    }
                }
            })
        @unknown default:
            print("ran into unknown case")
        }
    }
    
    
//    // Animation reference: https://stackoverflow.com/questions/31320819/scale-uibutton-animation-swift
    func changeRecordButton(to state: String) {
        playbackView.recordButton.setImage(UIImage(named: state), for: .normal)
        playbackView.recordButton.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)

        UIView.animate(withDuration: 0.8,
                                      delay: 0,
                                      usingSpringWithDamping: CGFloat(0.30),
                                      initialSpringVelocity: CGFloat(6.0),
                                      options: UIView.AnimationOptions.allowUserInteraction,
                                      animations: {
                                        self.playbackView.recordButton.transform = CGAffineTransform.identity
               },
                                      completion: { Void in()  }
           )
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
        
        songs = []
        tableView.reloadData()
        toggleEmptyState()
    }
    
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
   
    
    
    // MARK: - ARSCNViewDelegate
    
    /*
     // Override to create and configure nodes for anchors added to the view's session.
     func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
     let node = SCNNode()
     
     return node
     }
     */
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    

    private func setupLayout() {
        
        view.insertSubview(sphereButton, belowSubview: tableContainer)
        setUpSphereButton()
        view.insertSubview(torusButton, belowSubview: tableContainer)
        setUpTorusButton()
        view.insertSubview(abstractButton, belowSubview: tableContainer)
        setUpAbstractButton()
        view.insertSubview(clearButton, belowSubview: tableContainer)
        setUpClear()
        
        sphereButton.tintColor = UIColor(red:1.00, green:0.38, blue:0.27, alpha:1.0)
        
        musicSearch.searchBarStyle = UISearchBar.Style.minimal
        musicSearch.placeholder = " Find songs to play"
        musicSearch.searchTextField.layer.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.25).cgColor
        musicSearch.searchTextField.layer.cornerRadius = 7
        musicSearch.setSearchImage(color: .white)
        musicSearch.tintColor = UIColor(red:1.00, green:0.20, blue:0.45, alpha:1.0)
        
        musicSearch.searchTextField.textColor = .black
        musicSearch.searchTextField.font = .systemFont(ofSize: 16, weight: .medium)
        musicSearch.initPlaceholder(textColor: UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.7))

        musicSearch.delegate = self
        
        view.addSubview(musicSearch)
        musicSearch.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        musicSearch.translatesAutoresizingMaskIntoConstraints = false
        musicSearch.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        musicSearch.topAnchor.constraint(equalTo: view.topAnchor, constant: 40).isActive = true
        musicSearch.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.90).isActive = true
        
        whiteView.alpha = 0
        whiteView.backgroundColor = .white
        view.insertSubview(whiteView, belowSubview: musicSearch)
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(width: UIScreen.main.bounds.width, height: 48)
        gradient.colors = [UIColor.white.withAlphaComponent(0.99).cgColor, UIColor.white.withAlphaComponent(0.0).cgColor]
        
        whiteView.translatesAutoresizingMaskIntoConstraints = false
        whiteView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        whiteView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        whiteView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        whiteView.bottomAnchor.constraint(equalTo: musicSearch.bottomAnchor, constant: 24).isActive = true
        
        gradientView.alpha = 0
        gradientView.layer.addSublayer(gradient)
        view.insertSubview(gradientView, aboveSubview: whiteView)
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        gradientView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        gradientView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        gradientView.topAnchor.constraint(equalTo: whiteView.bottomAnchor, constant: -24).isActive = true
        gradientView.bottomAnchor.constraint(equalTo: whiteView.bottomAnchor).isActive = true
        gradientView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
    }
    
    let whiteView = UIView()
    let gradientView = UIView()
    
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // handle every letter inputted in to SearchBar, check if new letter (text) is new line letter (\n)
        if text == "\n" {
            doSearch()
            searchBar.resignFirstResponder()
        if let cancelButton : UIButton = searchBar.value(forKey: "cancelButton") as? UIButton{
            cancelButton.isEnabled = true
        }

        }
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
                UIView.animate(withDuration: 0.25, animations: {
                    self.tableContainer.alpha = 1
                    self.whiteView.alpha = 1
                    self.gradientView.alpha = 1
                    self.musicSearch.searchTextField.layer.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.10).cgColor
                    self.musicSearch.searchTextField.layer.cornerRadius = 7
                    self.musicSearch.changePlaceholderColor(textColor: UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5))
                    self.musicSearch.setSearchImage(color: .gray)
                })
            searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        // Remove focus from the search bar.
        searchBar.endEditing(true)
        songs = []
        tableView.reloadData()
        toggleEmptyState()
        
        DispatchQueue.main.async {
             UIView.animate(withDuration: 0.25, animations: {
                self.tableContainer.alpha = 0
                self.whiteView.alpha = 0
                self.gradientView.alpha = 0
                
                self.musicSearch.searchTextField.layer.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.25).cgColor
                self.musicSearch.searchTextField.layer.cornerRadius = 7
                self.musicSearch.changePlaceholderColor(textColor: .red)
                self.musicSearch.setSearchImage(color: .white)
             }) { _ in
                 
             }
         }
    }
    
    func doSearch() {
        SpotifyHelper.search(keyword: musicSearch.text!, successAction: { (songs) in
            // update tableView
            self.songs = songs
            self.tableView.reloadData()
            self.toggleEmptyState()
        }, failResponse: { err in
            // show alert about error
        })
    }
    
    func setupTableView() {
        tableContainer.alpha = 0
        tableContainer.backgroundColor = .white
        view.insertSubview(tableContainer, aboveSubview: whiteView)
        tableContainer.translatesAutoresizingMaskIntoConstraints = false
        tableContainer.topAnchor.constraint(equalTo: whiteView.bottomAnchor, constant: -24).isActive = true
        tableContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        tableContainer.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: tableContainer.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: tableContainer.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: tableContainer.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: tableContainer.rightAnchor).isActive = true
        tableView.backgroundColor = .white
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SongCell.self, forCellReuseIdentifier: "SongCell")
        
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func toggleEmptyState() {
        if songs.count < 1 {
            self.tableView.isHidden = true
        }
        else {
            self.tableView.isHidden = false
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = tableView.dequeueReusableCell(withIdentifier: "SongCell", for: indexPath) as! SongCell
        tableCell.setData(data: songs[indexPath.row])
        return tableCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        playbackView.playButton.alpha = 1
        let currentSong = songs[indexPath.row]
        guard let uri = currentSong.struri else { return }
        playSong(uri: uri)
        
        playbackView.setData(data: currentSong)
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.tableContainer.alpha = 0
            self?.whiteView.alpha = 0
            self?.gradientView.alpha = 0
        }
        
        songs = []
        tableView.reloadData()
        toggleEmptyState()
        
        //clear out search bar
        musicSearch.text = nil
        musicSearch.setShowsCancelButton(false, animated: true)
        musicSearch.endEditing(true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
}
// Reference https://stackoverflow.com/questions/13817330/how-to-change-inside-background-color-of-uisearchbar-component-on-ios
extension UISearchBar {

    func getTextField() -> UITextField? { return value(forKey: "searchField") as? UITextField }
    func initPlaceholder(textColor: UIColor) { searchTextField.initPlaceholder(textColor: textColor) }
    func changePlaceholderColor(textColor: UIColor) { searchTextField.changePlaceholderColor(textColor: textColor) }
    func setSearchImage(color: UIColor) {
         guard let imageView = searchTextField.leftView as? UIImageView else { return }
         imageView.tintColor = color
         imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
     }
}
private extension UITextField {
    private class Label: UILabel {
        private var _textColor = UIColor.lightGray
        override var textColor: UIColor! {
            set { super.textColor = _textColor }
            get { return _textColor }
        }
        init(label: UILabel, textColor: UIColor = .lightGray) {
            _textColor = textColor
            super.init(frame: label.frame)
            self.text = label.text
            self.font = label.font
        }
        required init?(coder: NSCoder) { super.init(coder: coder) }
    }
    var placeholderLabel: UILabel? { return value(forKey: "placeholderLabel") as? UILabel }
    func initPlaceholder(textColor: UIColor) {
        guard let placeholderLabel = placeholderLabel else { return }
        let label = Label(label: placeholderLabel, textColor: textColor)
        setValue(label, forKey: "placeholderLabel")
    }
    func changePlaceholderColor(textColor: UIColor) {
        placeholderLabel?.textColor = textColor
    }
}
