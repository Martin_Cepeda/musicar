import UIKit
class PlaybackControlsView: UIView {
    let playButton: UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "play"), for: .normal)
        button.layer.shadowOpacity = 0.2
        button.layer.shadowRadius = 8.0
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 0)
        button.alpha = 0
        return button
    }()
    
    var isPlaying = false
    
    let recordButton: UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "record"), for: .normal)
        button.layer.shadowOpacity = 0.2
        button.layer.shadowRadius = 8.0
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 0)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setData(data: Song) {
        isPlaying = true
        changePlayButton(to: "pause")
    }
    
    func changePlayButton(to state: String) {
        playButton.setImage(UIImage(named: state), for: .normal)
        playButton.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.6,
                                      delay: 0,
                                      usingSpringWithDamping: CGFloat(0.50),
                                      initialSpringVelocity: CGFloat(6.0),
                                      options: UIView.AnimationOptions.allowUserInteraction,
                                      animations: {
                                        self.playButton.transform = CGAffineTransform.identity
               },
                                      completion: { Void in()  }
           )
    }
    
    func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        addSubviews(views: recordButton, playButton)
        
        recordButton.square(edge: 80)
        recordButton.right(toAnchor: centerXAnchor, space: 40)
        recordButton.verticalSuperView(space: 24)
        
        playButton.leftHorizontalSpacing(toView: recordButton, space: 30)
        playButton.centerY(toView: recordButton)
        playButton.size(toView: recordButton)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
