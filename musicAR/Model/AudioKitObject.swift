import Foundation

import UIKit

import AudioKit
import AudioKitUI
import AVFoundation

class AudioKitObejct {
    var mic: AKMicrophone
    var tracker: AKFrequencyTracker
    var silence: AKBooster
    var fft: AKFFTTap
    
    init() {
        mic = AKMicrophone()!
        tracker = AKFrequencyTracker(mic)
        silence = AKBooster(tracker, gain: 0)
        fft = AKFFTTap(mic)
    }
    
}
