import Foundation
import Alamofire

class ApiClient {
    static func request(api: String,
                        method: HTTPMethod,
                        params: Parameters? = nil,
                        headers: HTTPHeaders? = nil,
                        successResponse: (AnyObject) -> Void,
                        failResponse: (Error) -> Void) {
        
        Alamofire.request(api, method: method, parameters: params, encoding: URLEncoding.default, headers: headers)
            .responseJSON { (responseData) in
                print(responseData)
        }
    }
    
}
