# Music AR
CSE 438 Fall 2019 
Group: Martin Cepeda, Jingqi Fan, Cheng Cheng Gao, Jack Zhai, Alex Huang

Our app allows AR Visualizations of sound. Users can generate AR objects in the app's camera view and log into their Spotify account to play songs. The AR objects will transform based on the sound from the song being played as well as any external sounds from the mic. 

## Group Work Distribution
UI & Animations: Martin Cepeda, Jingqi Fan
Visualizations: All 
Sound Analysis: Cheng Cheng Gao, Jack Zhai 
Networking: Jingqi Fan
Recording: Cheng Cheng Gao 
Architecture: Cheng Cheng Gao, Alex Huang 

## Frameworks & Libraries
 - AudioKit 
 - AlamoFire 
 - SceneKit 
 - MetalKit 
 - Spotify SDK
 - SceneKitVideo Recorder 
 - Lottie Animations 
 - SafariServices
 - AVFoundation 
 - Photos 

## Additional Attribution


- SCN Geometry
    - [https://medium.com/@maxxfrazer/arkit-scenekit-shaders-intro-99df65137006](https://medium.com/@maxxfrazer/arkit-scenekit-shaders-intro-99df65137006)
- Gradients:
    -   [https://anamontiel.com/](https://anamontiel.com/)
- Restricted to Portrait Mode
    -   [https://stackoverflow.com/questions/28938660/how-to-lock-orientation-of-one-view-controller-to-portrait-mode-only-in-swift](https://stackoverflow.com/questions/28938660/how-to-lock-orientation-of-one-view-controller-to-portrait-mode-only-in-swift)
- Wave Animation JSON File
    -   [https://lottiefiles.com/2671-sound-visualizer](https://lottiefiles.com/2671-sound-visualizer)
- AudioKit Playgrounds
    -   [https://audiokit.io/playgrounds/](https://audiokit.io/playgrounds/)
- Animation and UI
    -   [https://stackoverflow.com/questions/31320819/scale-uibutton-animation-swift](https://stackoverflow.com/questions/31320819/scale-uibutton-animation-swift)
    -   https://stackoverflow.com/questions/13817330/how-to-change-inside-background-color-of-uisearchbar-component-on-ios
- Spotify Integration
    -   [https://www.youtube.com/watch?v=KLsP7oThgHU](https://www.youtube.com/watch?v=KLsP7oThgHU)
    -  https://developer.spotify.com/documentation/ios/quick-start/
- Screen Recorder:
    -   https://github.com/svhawks/SceneKitVideoRecorder]
- Javascript Functions for easeInOutQuad:
    -   https://gist.github.com/gre/1650294]
    - (https://gist.github.com/gre/1650294)
- Alamofire Resources
    -  https://github.com/Alamofire/Alamofire

- KNConstrains for Layout Github file from Nguyen Truong Ky: 
    -  [https://github.com/nguyentruongky/KNConstraints](https://github.com/nguyentruongky/KNConstraints)

# How to Build 
**Prequisites**
- Spotify Developer account 
- Spotify Premium account  
- iPhone compatible with SceneKit
- iOS 13 + 
- XCode 11 + 

#### Step 1: Spotify Credentials
Create a spotify developer account. Register your project with its bundle identifier and keep track of your client ID and secret ID. 

####  Step 2: Configure File Paths 
When you download the project - the filepaths will be wrong. Some common problems will be: 
1. Wrong file path to Bridging Header. 
    - Go under Build Settings and search for Objective-C Briding Header and update the file path     to the correct one. 
    - The bridging header is called Music-AR.h
2. . Missing Frameworks 
    - If you have a missing framework make sure to download and update it in the correct place. Everything is installed via CocoaPods except AudioKit, which you'll need to drag into the project "Framework" folder if missing. 
    - Make sure to have CocoaPods installed. 
    - Make sure to build in your .workspace not your .project. Update to correct file paths inside both. 

#### Step 3: Update Spotify Credentials 
Under "Model/SpotifyHelper.swift" change the client_id and client_secret to the ones you generated from  your Spotify developer dashboard. 

