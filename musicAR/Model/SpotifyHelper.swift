import Foundation
import Alamofire

class SpotifyHelper {
    // save token into Keychain --> dont have to getotken every time
    static var token: String?
    static func getToken( successAction:( () -> Void)? = nil) {
        let parameters = ["client_id" : "99521960b05b48e991b4865cbf39318f",
                          "client_secret" : "8d13d8e34a2f47e6aae080a6b34ffd88",
                          "grant_type" : "client_credentials"]
        Alamofire.request("https://accounts.spotify.com/api/token", method: .post, parameters: parameters).responseJSON(completionHandler: {
            response in
            
            if let result = response.result.value {
                let jsonData = result as! NSDictionary
                token = jsonData.value(forKey: "access_token") as? String
                successAction?()
            }
        })
    }
    
    static func search(keyword: String, successAction:( ([Song]) -> Void)?, failResponse: ((Error) -> Void)?) {
        guard let token = token else {
            getToken(successAction: {
                search(keyword: keyword, successAction: successAction, failResponse: failResponse)
            })
            return
        }
        
        let finalKeywords = keyword.replacingOccurrences(of: " ", with: "+")
        
        let searchURL = "https://api.spotify.com/v1/search?q=\(finalKeywords)&type=track&&limit=15&access_token=\(token)"
        Alamofire.request(searchURL, method: .get)
            .responseJSON(completionHandler: {
                response in
                
                guard let data = response.data else { return }
                do {
                    let readableJSON = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as AnyObject
                    
                    guard let tracks = readableJSON["tracks"] as AnyObject? else { return }
                    guard let items = tracks["items"] as? [AnyObject] else { return }
                    let songs = items.map({ return Song(raw: $0) })
                    successAction?(songs)
                }
                catch let err {
                    failResponse?(err)
                }
            })
    }
}


class Song {
    
    var name:String?
    var type:String?
    var struri:String?
    var artists = [Artist]()
    
    var thumbnailUrlString: URL?
    
    init(raw: AnyObject) {
        name = raw["name"] as? String
        type = raw["type"] as? String
        struri = raw["uri"] as? String
        
        if let subRaw = raw["artists"] as? [AnyObject] {
            artists = subRaw.map({ return Artist(raw: $0) })
        }
        
        if let subRaw = raw.value(forKeyPath: "album.images") as? [AnyObject] {
            if let first = subRaw.first,
                let urlString = first["url"] as? String,
                let url = URL(string: urlString) {
                thumbnailUrlString = url
            }
        }
        //
        //        if let _ = raw["preview_url"] as? String
        //        {
        //            if let album = raw["album"] as AnyObject? {
        //                if let images = album["images"] as? [AnyObject]{
        //                    let imageData = images[0]
        //                    thumbnailUrlString =  URL(string: imageData["url"] as! String)
        //                }
        //            }
        //        }
    }
}

class Artist {
    var name: String?
    init(raw: AnyObject) {
        name = raw["name"] as? String
    }
}


import Kingfisher
extension UIImageView {
    func downloadImage(from url: String?, placeholder: UIImage? = nil, completion: ((UIImage?) -> Void)? = nil) {
        guard let url = url, let nsurl = URL(string: url) else { return }
        alpha = 0
        kf.setImage(with: ImageResource(downloadURL: nsurl), placeholder: placeholder, completionHandler: { (image, _, _, _) in
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.5, animations: {
                    self.alpha = 1.0
                })
            }
            
            completion?(image)
        })
    }
}
