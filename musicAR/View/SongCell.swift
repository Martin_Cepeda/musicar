import Foundation
import UIKit

class SongCell: UITableViewCell {
    
    // here is just for define
    var albumImageView = UIImageView()
    var albumNameLabel = UILabel()
    var albumArtistNameLabel = UILabel()
    
    
    func setup() {
        addSubview(albumImageView)
        
        albumImageView.contentMode = .scaleAspectFill
        albumImageView.layer.cornerRadius = 5
        albumImageView.clipsToBounds = true
        albumImageView.backgroundColor = UIColor.gray
        
        albumNameLabel.numberOfLines = 2
        albumNameLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        
        albumArtistNameLabel.numberOfLines = 2
        albumArtistNameLabel.font = UIFont.systemFont(ofSize: 14)
        albumArtistNameLabel.textColor = .lightGray
        
        albumImageView.translatesAutoresizingMaskIntoConstraints = false
        albumImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        albumImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        albumImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        albumImageView.widthAnchor.constraint(equalTo: albumImageView.heightAnchor).isActive = true
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubviews(views: albumNameLabel, albumArtistNameLabel)
        view.stackVertically(views: [albumNameLabel, albumArtistNameLabel], viewSpaces: 4, topSpace: 0, bottomSpace: 0  )
        albumNameLabel.horizontalSuperView()
        albumArtistNameLabel.horizontalSuperView()
        
        addSubviews(views: view)
        view.leftHorizontalSpacing(toView: albumImageView, space: 16)
        view.rightToSuperview(space: -16)
        view.centerYToSuperview()
    }
    
    func setData(data: Song) {
        albumImageView.downloadImage(from: data.thumbnailUrlString?.absoluteString)
        albumNameLabel.text = data.name
        let names = data.artists.compactMap({ return $0.name })
        albumArtistNameLabel.text = names.joined(separator: ", ")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
