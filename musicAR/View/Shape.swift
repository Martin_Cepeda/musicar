import Foundation
import UIKit
import SceneKit
import ARKit
import AudioKit
import AudioKitUI

class Shape {
    var sphereArray: [SCNNode]
    var torusArray: [SCNNode]
    var shadersArray: [SCNNode]
    var tracker: AKFrequencyTracker
    
    init(tracker_: AKFrequencyTracker) {
        sphereArray = [SCNNode]()
        torusArray = [SCNNode]()
        shadersArray = [SCNNode]()
        tracker = tracker_
    }
    
    var gradients:[UIImage] = [UIImage(named:"1.JPG")!, UIImage(named:"2.JPG")!, UIImage(named:"3.JPG")!, UIImage(named:"4.JPG")!, UIImage(named:"5.JPG")!, UIImage(named:"6.JPG")!, UIImage(named: "7.JPG")!, UIImage(named: "8.JPG")!]
    
    func createSphere(position: SCNVector3) -> SCNNode {
        let sphereShape = SCNSphere(radius: 0.02)
        sphereShape.isGeodesic = true
        sphereShape.segmentCount = 50
        sphereShape.firstMaterial?.diffuse.contents = gradients.randomElement()
        sphereShape.firstMaterial?.isDoubleSided = true
        let sphereNode = SCNNode(geometry: sphereShape)
        sphereNode.position = position
        sphereArray.append(sphereNode)
        return sphereNode
    }
    
    
    func createTorus(position: SCNVector3) -> SCNNode {
        let torusShape = SCNTorus(ringRadius: 0.05, pipeRadius: 0.01)
        let torusNode = SCNNode(geometry: torusShape)
        torusNode.position = position
        torusArray.append(torusNode)
        torusNode.geometry?.firstMaterial?.diffuse.contents = gradients.randomElement()
        return torusNode
    }
    let colorVar = [[21, 0, 255, 1], [161, 255, 0, 1], [255, 0, 0, 1]]
    
    func createAbstract(position: SCNVector3) -> SCNNode {
        let abstractGeometry = SCNSphere(radius: 0.05)
        abstractGeometry.shaderModifiers = [
            .surface: getShader(from: "sphere_surface"),
            .geometry: getShader(from: "sphere_geometry")
        ]
        
        let abstractNode = SCNNode(geometry: abstractGeometry)
        abstractNode.scale = SCNVector3(0.5, 0.5, 0.5)
        let chosenIndex = colorVar.randomElement()
        abstractGeometry.setValue(chosenIndex?[0], forKey: "red")
        abstractGeometry.setValue(chosenIndex?[1], forKey: "green")
        abstractGeometry.setValue(chosenIndex?[2], forKey: "blue")
        
        abstractNode.position = position
        shadersArray.append(abstractNode)
        return abstractNode
        
    }
    
    
    
    private func getShader(from filename: String) -> String {
        do {
            if let dirs = Bundle.main.url(forResource: filename, withExtension: "shader") {
                return try String(contentsOf: dirs, encoding: .utf8)
            }
        } catch {
            print(error)
        }
        print("shader \(filename) not found")
        return ""
    }
    
    //modifiers to objects
    
    func easeInOutQuad(val: Double) -> CGFloat { return CGFloat( val < 0.5 ? 2 * val * val : -1 + ( 4 - 2 * val ) * val) }
    
    func rotateTorus(collectionAR: [SCNNode])  {
        for object in collectionAR {
            let rotateTorusNode = SCNAction.rotate(by: CGFloat(tracker.frequency) / 2 * CGFloat((.pi)/180.0), around: SCNVector3(x:1, y:1, z:2), duration: 2)
            object.runAction(rotateTorusNode)
        }
    }
    
    func rotateObject(collectionAR: [SCNNode]) {
        for object in collectionAR {
            let rotation = SCNAction.rotate(by: 360 * CGFloat((.pi)/180.0), around: SCNVector3(x:0, y:1, z:0), duration: 0.5)
            rotation.speed = CGFloat(rotationSpeed(frequency: tracker.frequency, amplitude: tracker.amplitude))
            let reverseRotation = rotation.reversed()
            let rotationSequence = SCNAction.sequence([rotation, reverseRotation])
            object.runAction(rotationSequence)
        }
        
    }
    
    func hoverUpDown(collectionAR: [SCNNode]) {
        for object in collectionAR {
            let hoverUp = SCNAction.moveBy(x: 0, y: easeInOutQuad(val: tracker.amplitude), z: 0, duration: 0.25)
            let hoverDown = hoverUp.reversed()
            let hoverSequence = SCNAction.sequence([hoverUp, hoverDown])
            hoverSequence.duration = 1
            object.runAction(hoverSequence)
        }
    }
    
    func makeLarge (collectionAR: [SCNNode]) {
        for object in collectionAR {
            let scaleUp = SCNAction.scale(by: CGFloat(sizeScale(frequency: tracker.frequency, amplitude: tracker.amplitude)), duration: 0.25 )
            let scaleDown = scaleUp.reversed()
            let scaleSequence = SCNAction.sequence([scaleUp, scaleDown])
            object.runAction(scaleSequence)
        }
    }
    
    func hoverLength(amplitude: Double) -> CGFloat {
        var hoverLength = CGFloat(amplitude * 1.5)
        
        if(hoverLength > 2) {
            hoverLength = CGFloat(2)
        }
        else if(amplitude < 0.03) {
            hoverLength = CGFloat(0)
        }
        else {
            hoverLength = CGFloat(amplitude * 2)
        }
        return hoverLength
    }
    
    func sizeScale(frequency: Double, amplitude: Double) -> CGFloat {
        var sizeScale = CGFloat(amplitude * 20)
        if sizeScale >= 3 {
            sizeScale = CGFloat(3.0)
        }
        else if amplitude < 0.02 {
            sizeScale = CGFloat(1)
        }
        else {
            sizeScale = CGFloat(amplitude * 20)
        }
        return sizeScale
    }
    
    func rotationSpeed(frequency: Double, amplitude: Double) -> CGFloat {
        let rotateSpeed = easeInOutQuad(val:sqrt(frequency * amplitude))
        return rotateSpeed
    }
    func changingAbstract(collectionAR: [SCNNode]) {
        for abstractNode in collectionAR {
            abstractNode.geometry?.setValue(tracker.amplitude * 15, forKey: "x")
        }
    }
    
}
