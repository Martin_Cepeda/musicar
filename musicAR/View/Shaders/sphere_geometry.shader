//
//  sphere_geometry.shader.swift
//  mictest
//
//  Created by Marty  on 11/28/19.
//  Copyright © 2019 zhai. All rights reserved.
//

 float theta1 = atan2(_geometry.position.x, _geometry.position.y);
 float theta2 = atan2(_geometry.position.x, _geometry.position.z);
 float pi = 3.14159;
 uniform float x;

_geometry.position.xyz += _geometry.position.xyz * x
    * sin(theta1 * pi - u_time * 3 ) * sin(6.0 * theta1)
    * cos(7.0 * theta2);
    
