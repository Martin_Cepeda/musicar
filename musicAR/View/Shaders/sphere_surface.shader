#pragma transparent
#pragma body

float dotProductEdge = 0.5;

float dotProduct = dot(_surface.view, _surface.normal);
dotProduct = dotProduct < 0.0 ? 0.0 : dotProduct;

uniform float red;
uniform float green;
uniform float blue;
uniform sampler2D texture;

_surface.diffuse.rgba = vec4(red, green, blue, 0.5);

